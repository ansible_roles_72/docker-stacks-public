#!/bin/bash
#usage bash scale_pxc.sh "passwordMYSmjgfdgkhl3hflgdfskgjuhgflsgklhjfsg" "3"
MYSQL_ROOT_PASSWORD="$1"
SCALE_SERVICE_COUNT=3
#MYSQL_ROOT_PASSWORD="passwordMYSmjgfdgkhl3hflgdfskgjuhgflsgklhjfsg"
###maxcounter=18
### 
###counter=1
######while ! docker exec -i $(docker ps | grep proxysql | awk '{ print $NF}' ) mysql -h pxc -uroot  -p${MYSQL_ROOT_PASSWORD}  -P3306 -e 'show ###databases;' > /dev/null 2>&1; do
    ###sleep 10
    ###counter=`expr $counter + 1`
    ###if [ $counter -gt $maxcounter ]; then
        ###>&2 echo "We have been waiting for Percona too long already; failing."
        ###exit 1
    ###fi;
###
###done
sleep 120;
PXC_NODES=$(docker service ls | grep pxc-consul | awk '{ print $4 }' | cut -d\/ -f2)
echo "PXC_NODES is ${PXC_NODES}"
if [[ "${PXC_NODES}" != "${SCALE_SERVICE_COUNT}" ]];then
		echo "scale cluster to ${SCALE_SERVICE_COUNT}"
	  docker service scale pxc_pxc=${SCALE_SERVICE_COUNT} -d
	  echo "wait util nodes sync"
	  sleep 80;
else
	 echo "service already scaled to ${SCALE_SERVICE_COUNT}"
fi


docker exec -i $(docker ps | grep proxysql | awk '{ print $NF }' ) add_cluster_nodes.sh

PXC_CLUSTER_STATUS=$(docker exec -i $(docker ps | grep proxysql | awk '{ print $NF}' ) mysql -u admin -padmin -h 127.0.0.1 -P6032 --prompt='Admin> ' -e "select * from stats.stats_mysql_connection_pool";)

echo "PXC_CLUSTER_STATUS is"
echo "${PXC_CLUSTER_STATUS}"

