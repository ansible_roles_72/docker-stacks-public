DATABASES = {
    'default': {
        'ATOMIC_REQUESTS': True,
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': "awx",
        'USER': "awx",
        'PASSWORD': "awxpasswordnew",
        'HOST': "awx-postgres",
        'PORT': "15432",
    }
}

BROADCAST_WEBSOCKET_SECRET = "SG5oWWwtSW54b1ZabDd6ZXBIaVM="
